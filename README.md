# Topics in Multimedia Systems Project 2015 #

Implementing a prototype IBR resolution service, based on [this](http://pages.cs.wisc.edu/~akella/papers/ibr-conext13.pdf) publication

## Services ##

+   IBR Service


Resolves IBRs to image locations. Various metadata are associated with each location(size, encoding e.t.c) 

LSH hashing based on [this](https://people.csail.mit.edu/indyk/p117-andoni.pdf)  {Page 3}

+   Crawler Service


Scan the web using Selenium


+   Perceptual Hash Service


Custom perceptual hash as described in the IBR publication


+   Viewer Service


Simple client showcasing the ability for applications to implement their own logic on image selection through IBR resolution

# Developers #
Koutsoulis Vasileios, MSc AUEB Computer Science

Mitsopoulou Elena, MSc AUEB Computer Science

Vitsas Nikolaos, MSc AUEB Computer Science